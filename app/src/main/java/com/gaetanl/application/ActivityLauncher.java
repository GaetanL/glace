package com.gaetanl.application;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.gaetanl.controller.ContactActivity;
import com.gaetanl.controller.MainActivity;

/**
 * Helper singleton for activity starting, allowing to enforce required parameters before starting
 * an activity
 * Every activity should be created here, apart from InitActivity where the singleton is initialized
 */
public class ActivityLauncher {
    private static ActivityLauncher INSTANCE = null;
    private static Object mutex = new Object();

    private ActivityLauncher() {}

    public static ActivityLauncher getInstance() {
        if (INSTANCE == null) {
            synchronized (mutex) {
                if (INSTANCE == null) {
                    INSTANCE = new ActivityLauncher();
                }
            }
        }

        return INSTANCE;
    }

    private Intent buildIntent(Context context, Class<? extends Context> activityToStart, Bundle bundle) {
        Intent intent = new Intent(context, activityToStart);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        return intent;
    }

    public void startMainActivity(Context context) {
        Intent intent = buildIntent(context, MainActivity.class, null);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public void startContactCreateActivity(Context context) {
        Bundle bundle = new Bundle();
        bundle.putInt("mode", ContactActivity.Mode.NEW.ordinal());
        Intent intent = buildIntent(context, ContactActivity.class, bundle);
        context.startActivity(intent);
    }

    public void startContactActivity(Context context, long id) {
        Bundle bundle = new Bundle();
        bundle.putInt("mode", ContactActivity.Mode.EXISTING.ordinal());
        bundle.putLong("id", id);
        Intent intent = buildIntent(context, ContactActivity.class, bundle);
        context.startActivity(intent);
    }
}
