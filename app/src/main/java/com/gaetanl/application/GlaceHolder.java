package com.gaetanl.application;

import android.content.Context;
import android.support.annotation.NonNull;

import com.gaetanl.domain.Contact;
import com.gaetanl.domain.Lazy;
import com.gaetanl.persistence.GlacePersistenceManager;
import com.gaetanl.persistence.PersistenceManager;
import com.gaetanl.util.Callback;

import java.util.Collection;

/**
 * Application singleton
 *
 * @author Gaetan Leu
 */
public class GlaceHolder implements GlHolder {
    private PersistenceManager persistenceManager;
    public Lazy<Collection<Contact>> contacts;



    // Singleton instance management
    private static GlaceHolder INSTANCE = null;
    private static Object mutex = new Object();

    private GlaceHolder() {
        super();
    }

    /**
     * Lazy-loaded synchronized singleton insuring there is only one instance of the application
     * @return the application instance
     */
    public static GlaceHolder getInstance() {
        if (INSTANCE == null) {
            synchronized (mutex) {
                if (INSTANCE == null) {
                    INSTANCE = new GlaceHolder();
                }
            }
        }

        return INSTANCE;
    }

    @Override
    public void reset(@NonNull Context applicationContext) {

    }

    @Override
    public void initialize(@NonNull Context applicationContext, @NonNull Callback<Void> callback) {
        persistenceManager = new GlacePersistenceManager(applicationContext);
        contacts = persistenceManager.getLazyCollectionInstance(Contact.class);
        callback.onResponse(null);
    }

    public PersistenceManager getPersistenceManager() {
        return persistenceManager;
    }
}
