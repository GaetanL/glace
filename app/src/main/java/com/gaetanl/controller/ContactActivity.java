package com.gaetanl.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gaetanl.application.GlaceHolder;
import com.gaetanl.domain.Contact;
import com.gaetanl.glace.R;
import com.gaetanl.util.Callback;

import static com.gaetanl.controller.ContactActivity.Mode.EXISTING;

/**
 * Created by gaetan.leu on 06/06/2017.
 */
public class ContactActivity extends Activity {
    public enum Mode {
        NEW,
        EXISTING
    };
    private Mode mode;
    private Contact contact;

    TextView contactNameView;
    Button submitButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contact);

        contactNameView = (TextView) findViewById(R.id.contact_name);
        submitButton = (Button) findViewById(R.id.submit);

        Bundle bundle = getIntent().getExtras();
        mode = Mode.values()[bundle.getInt("mode")];
        if (EXISTING.equals(mode)) {
            long id = bundle.getLong("id");
            if (id == 0) {
                throw new IllegalStateException(); // TODO
            }
            else {
                GlaceHolder.getInstance().getPersistenceManager().read(Contact.class, id, new Callback<Contact>() {
                    @Override
                    public void onResponse(Contact contact) {
                        ContactActivity.this.contact = contact;
                        contactNameView.setText(contact.getDisplayedName());
                    }

                    @Override
                    public void onError(@NonNull Throwable throwable) {
                        throw new IllegalStateException(); // TODO
                    }
                });
            }
        }

        @StringRes Integer submitTextResId = null;
        switch (mode) {
            case NEW:
                submitTextResId = R.string.create;
                break;

            case EXISTING:
                submitTextResId = R.string.update;
                break;
        }
        submitButton.setText(submitTextResId);
    }

    public void submit(View view) {
        String contactName = contactNameView.getText().toString().trim();

        Callback<Void> submitCallback = new Callback<Void>() {
            @Override
            public void onResponse(Void aVoid) {
                finish();
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                throw new IllegalStateException(); // TODO
            }
        };

        if (!contactName.isEmpty()) {
            switch (mode) {
                case NEW:
                    contact = new Contact(contactName);
                    createContact(contact, submitCallback);
                    finish();
                    break;

                case EXISTING:
                    contact.setDisplayedName(contactName);
                    updateContact(contact, submitCallback);
                    break;
            }
        }
    }

    public void createContact(Contact contact, final Callback<Void> callback) {
        final GlaceHolder glaceHolder = GlaceHolder.getInstance();

        glaceHolder.getPersistenceManager().create(contact, new Callback<Long>() {
            @Override
            public void onResponse(Long aLong) {
                glaceHolder.contacts.setLocalObjectObsolete();
                callback.onResponse(null);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                callback.onError(throwable);
            }
        });
    }

    public void updateContact(Contact contact, final Callback<Void> callback) {
        final GlaceHolder glaceHolder = GlaceHolder.getInstance();

        glaceHolder.getPersistenceManager().update(contact, new Callback<Void>() {
            @Override
            public void onResponse(Void aVoid) {
                glaceHolder.contacts.setLocalObjectObsolete();
                callback.onResponse(null);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                callback.onError(throwable);
            }
        });
    }
}
