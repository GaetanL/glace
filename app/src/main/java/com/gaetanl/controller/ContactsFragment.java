package com.gaetanl.controller;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.gaetanl.application.ActivityLauncher;
import com.gaetanl.application.GlaceHolder;
import com.gaetanl.domain.HasTextComparator;
import com.gaetanl.domain.Contact;
import com.gaetanl.glace.R;
import com.gaetanl.presentation.SwitchableModeListView;
import com.gaetanl.presentation.adapter.AlphaSideIndexManager;
import com.gaetanl.util.Callback;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Displays a list of contacts
 *
 * @author Gaetan Leu
 */
public class ContactsFragment extends Fragment {
    private MainActivity context;
    private AlphaSideIndexManager<Contact> sideIndexManager;
    public SwitchableModeListView listView;
    private List<Contact> localContacts;
    private ArrayList<Boolean> selectedMap = new ArrayList<Boolean>();

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Contact clickedContact = sideIndexManager.get(position);
            if (clickedContact != null) {
                ActivityLauncher.getInstance().startContactActivity(context, clickedContact.getId());
            }
        }
    };
    AdapterView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            final Contact clickedContact = sideIndexManager.get(position);
            if (clickedContact != null) {
                final GlaceHolder glaceHolder = GlaceHolder.getInstance();

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                glaceHolder.getPersistenceManager().delete(clickedContact, new Callback<Void>() {
                                    @Override
                                    public void onResponse(Void aVoid) {
                                        glaceHolder.contacts.setLocalObjectObsolete();
                                        updateList();
                                    }

                                    @Override
                                    public void onError(@NonNull Throwable throwable) {
                                        throw new IllegalStateException(); // TODO
                                    }
                                });
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder
                        .setMessage(String.format(getString(R.string.confirm_delete), clickedContact.getDisplayedName()))
                        .setPositiveButton(R.string.yes, dialogClickListener)
                        .setNegativeButton(R.string.no, dialogClickListener)
                        .show();
            }
            return true;
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = (MainActivity) context;
        ((MainActivity) context).setCurrentFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_main_fragment_contacts, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView = (SwitchableModeListView) view.findViewById(android.R.id.list);
        LinearLayout sideIndex = (LinearLayout) view.findViewById(R.id.sideIndex);

        listView.setSelectedMap(selectedMap);
        listView.setDisplayModeListener(onItemClickListener);
        AlphaSideIndexManager alphaSideIndexManager = new AlphaSideIndexManager<>(context, Contact.class, localContacts, new HasTextComparator(context, Contact.class), view, sideIndex);
        alphaSideIndexManager.setSwitchableListView(listView);
        sideIndexManager = alphaSideIndexManager;
    }

    @Override
    public void onResume() {
        super.onResume();

        updateList();
        context.invalidateOptionsMenu();
    }

    private void updateList() {
        GlaceHolder.getInstance().contacts.get(new Callback<Collection<Contact>>() {
            @Override
            public void onResponse(Collection<Contact> contacts) {
                // updating only if first time loading or if list has been updated
                List<Contact> retreivedContacts = new ArrayList<>(contacts);
                Collections.sort(retreivedContacts, new Comparator<Contact>() {
                    @Override
                    public int compare(Contact o1, Contact o2) {
                        return o1.getDisplayedName().compareTo(o2.getDisplayedName());
                    }
                });
                if (localContacts == null || !localContacts.equals(retreivedContacts)) {
                    localContacts = retreivedContacts;
                    sideIndexManager.updateList(localContacts);
                }
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                throw new IllegalStateException(); // TODO
            }
        });
    }
}
