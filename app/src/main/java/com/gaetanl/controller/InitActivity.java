package com.gaetanl.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.gaetanl.application.ActivityLauncher;
import com.gaetanl.application.GlaceHolder;
import com.gaetanl.util.Callback;

/**
 * Initialization of the application
 *
 * @author Gaetan Leu
 */
public class InitActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GlaceHolder.getInstance().initialize(getApplicationContext(), new Callback<Void>() {
            @Override
            public void onResponse(Void aVoid) {
                ActivityLauncher.getInstance().startMainActivity(InitActivity.this);
                finish();
            }

            @Override
            public void onError(Throwable throwable) {
                throw new IllegalStateException(throwable); // TODO
            }
        });
    }
}
