package com.gaetanl.controller;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gaetanl.application.ActivityLauncher;
import com.gaetanl.domain.HasDrawable;
import com.gaetanl.domain.HasText;
import com.gaetanl.glace.R;
import com.gaetanl.presentation.GraphicsBuilder;
import com.gaetanl.presentation.UiBuilder;
import com.gaetanl.presentation.adapter.TextIconItem;
import com.gaetanl.presentation.adapter.TextItem;

import java.util.ArrayList;

import static android.view.MenuItem.SHOW_AS_ACTION_IF_ROOM;
import static com.gaetanl.presentation.SwitchableModeListView.Mode.DISPLAY;
import static com.gaetanl.presentation.SwitchableModeListView.Mode.SELECTION;

/**
 * Application entry point
 *
 * @author Gaetan Leu
 */
public class MainActivity extends Activity {
    private ArrayList<HasText> mainMenu;
    private ArrayList<HasText> optionMenu;
    private DrawerLayout drawerLayout;
    private ListView listView;
    private Fragment currentFragment = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainMenu = new ArrayList<>();
        mainMenu.add(new TextIconItem(R.string.contact_add, R.drawable.icon_contact_add));
        mainMenu.add(new TextIconItem(R.string.import_export, R.drawable.icon_import_export));
        mainMenu.add(new TextIconItem(R.string.manage_tags, R.drawable.icon_tag));
        mainMenu.add(new TextIconItem(R.string.settings, R.drawable.icon_settings));

        optionMenu = new ArrayList<>();
        optionMenu.add(new TextItem("Titi"));
        optionMenu.add(new TextIconItem(R.string.contact_add, R.drawable.icon_contact_add));
        optionMenu.add(new TextItem("Tutu"));

        setContentView(R.layout.activity_main);
        drawerLayout = ((DrawerLayout) findViewById(R.id.main_drawer));
        listView = (ListView) findViewById(R.id.main_menu);

        UiBuilder uiBuilder = new UiBuilder(this);
        uiBuilder.buildDrawerMenu(R.id.main_menu, mainMenu);
        uiBuilder.buildToolbar(R.id.main_toolbar, R.id.main_drawer);
        listView.setOnItemClickListener(onMenuItemClickListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        new UiBuilder(this).buildToolbarOptionMenu(menu, optionMenu, R.style.AppToolbarTheme);
        return super.onPrepareOptionsMenu(menu);
    }
    public int getResourceFromStyle(@StyleRes int styleResId, int resId) {
        int[] attrs = {resId};
        TypedArray ta = obtainStyledAttributes(styleResId, attrs);
        int result = ta.getInt(0, -7777);
        ta.recycle();
        return result;
    }
    @Override
    public void onBackPressed() {
        if (currentFragment.getClass().equals(ContactsFragment.class) && SELECTION.equals(((ContactsFragment) currentFragment).listView.getMode())) {
            ((ContactsFragment) currentFragment).listView.setMode(DISPLAY);
        }
        else {
            super.onBackPressed();
        }
    }

    private AdapterView.OnItemClickListener onMenuItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            drawerLayout.closeDrawer(GravityCompat.START);
            final int _position = position;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    switch (_position) {
                        case 0:
                            ActivityLauncher.getInstance().startContactCreateActivity(MainActivity.this);
                            break;
                    }
                }
            }, 200);
        }
    };

    public void setCurrentFragment(Fragment currentFragment) {
        this.currentFragment = currentFragment;
    }
}
