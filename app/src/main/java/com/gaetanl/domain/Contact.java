package com.gaetanl.domain;

import android.content.Context;

/**
 * A contact can be a person or a service
 *
 * @author Gaetan Leu
 */
public class Contact extends BusinessObject implements HasText {
    private String displayedName;

    public Contact(String displayedName) {
        this(null, displayedName);
    }

    public Contact(Long id, String displayedName) {
        super(id);
        this.displayedName = displayedName;
    }

    public String getDisplayedName() {
        return displayedName;
    }

    public void setDisplayedName(String displayedName) {
        this.displayedName = displayedName;
    }

    @Override
    public String getText(Context context) {
        return displayedName;
    }
}
