package com.gaetanl.domain;

import android.content.Context;

import java.util.Comparator;

/**
 * Generic comparator for HasText
 *
 * @author Gaetan Leu
 */
public class HasTextComparator implements Comparator<HasText> {
    private final Context context;
    private final Class<? extends HasText> objectClass;

    public HasTextComparator(Context context, Class<? extends HasText> objectClass) {
        this.context = context;
        this.objectClass = objectClass;
    }

    @Override
    public int compare(HasText o1, HasText o2) {
        if (objectClass.equals(Contact.class)) {
            return ((Contact) o1).getDisplayedName().compareTo(((Contact) o2).getDisplayedName());
        }
        else {
            return o1.getText(context).compareTo(o2.getText(context));
        }
    }
}
