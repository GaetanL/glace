package com.gaetanl.persistence;

import android.support.annotation.NonNull;

import com.gaetanl.domain.Contact;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Dao for Contact BusinessObject
 *
 * @author Gaetan Leu
 */
public interface ContactDao extends Dao<Contact> {
    public static FieldsDefinition FIELDS_DEFINITION = new FieldsDefinition() {
        @Override
        public @NonNull Map<String, FieldType> get() {
            Map<String, FieldType> fields = new LinkedHashMap<>();
            fields.put("displayedName", FieldType.TEXT);
            return fields;
        }
    };

    public static FieldsValueMapper<Contact> FIELDS_VALUE_MAPPER = new FieldsValueMapper<Contact>() {
        @Override
        public @NonNull Map<String, Object> map(@NonNull Contact object) {
            Map<String, Object> fieldValues = new LinkedHashMap<>();
            fieldValues.put("displayedName", object.getDisplayedName());
            return fieldValues;
        }
    };

    public static String ENTITY_NAME = "Contact";
}
