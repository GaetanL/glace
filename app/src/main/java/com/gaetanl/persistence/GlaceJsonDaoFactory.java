package com.gaetanl.persistence;

import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;
import com.gaetanl.domain.Contact;

/**
 * Glace JsonDaoFactory implementation
 *
 * @author Gaetan Leu
 */
public class GlaceJsonDaoFactory extends JsonDaoFactory {
    @Override
    protected @NonNull JsonDao<? extends BusinessObject> getJsonDao(@NonNull Class<? extends BusinessObject> objectClass) {
        JsonDao<? extends BusinessObject> dao = null;

        if (objectClass.equals(Contact.class)) {
            dao = JsonContactDao.getInstance();
        }

        return dao; // TODO check null
    }
}
