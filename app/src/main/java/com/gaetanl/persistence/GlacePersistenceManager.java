package com.gaetanl.persistence;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Glace PersistentManager implementation
 *
 * @author Gaetan Leu
 */
public class GlacePersistenceManager extends PersistenceManager {
    public GlacePersistenceManager(@NonNull Context context) {
        super(context, new GlaceSqliteDaoFactory(context, "glace", 1));
    }
}
