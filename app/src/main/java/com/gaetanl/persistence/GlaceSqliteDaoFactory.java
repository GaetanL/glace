package com.gaetanl.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.gaetanl.domain.BusinessObject;
import com.gaetanl.domain.Contact;

import java.util.Map;

/**
 * Glace SqliteDaoFactory implementation
 *
 * @author Gaetan Leu
 */
public class GlaceSqliteDaoFactory extends SqliteDaoFactory {
    public GlaceSqliteDaoFactory(@NonNull Context context, @NonNull String databaseName, int databaseVersion) {
        super(context, databaseName, databaseVersion);
    }

    @Override
    protected @NonNull SqliteDao<? extends BusinessObject> getSqliteDao(@NonNull Class<? extends BusinessObject> objectClass) {
        SqliteDao<? extends BusinessObject> dao = null;

        if (objectClass.equals(Contact.class)) {
            dao = SqliteContactDao.getInstance(this);
        }

        return dao; // TODO check null
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String tableDescription = " id INTEGER PRIMARY KEY AUTOINCREMENT ";

        Map<String, FieldType> fields = ContactDao.FIELDS_DEFINITION.get();
        for (Map.Entry<String, FieldType> field: fields.entrySet()) {
            tableDescription += ", " + field.getKey();
            switch (field.getValue()) {
                case BOOLEAN:
                    tableDescription += " BOOLEAN ";
                    break;
                case INTEGER:
                    tableDescription += " INTEGER ";
                    break;
                case TEXT:
                    tableDescription += " TEXT ";
                    break;
            }
        }

        db.execSQL(String.format("CREATE TABLE %s(%s)", ContactDao.ENTITY_NAME, tableDescription));
    }

    @Override
    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(String.format("DROP TABLE IF EXISTS %s", ContactDao.ENTITY_NAME));
        this.onCreate(db);
    }
}
