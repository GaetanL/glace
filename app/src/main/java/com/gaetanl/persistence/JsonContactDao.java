package com.gaetanl.persistence;

import android.support.annotation.NonNull;

import com.gaetanl.domain.Contact;
import com.gaetanl.domain.Lazy;
import com.gaetanl.util.Callback;

import java.util.Collection;
import java.util.Map;

/**
 * Contact JSON concrete Dao implementation
 *
 * @author Gaetan Leu
 */
public class JsonContactDao extends JsonDao<Contact> implements ContactDao {
    private static JsonContactDao INSTANCE = null;
    private static Object mutex = new Object();

    private JsonContactDao() {
        super();
    }

    public static JsonContactDao getInstance() {
        if (INSTANCE == null) {
            synchronized (mutex) {
                if (INSTANCE == null) {
                    INSTANCE = new JsonContactDao();
                }
            }
        }

        return INSTANCE;
    }

    @Override
    public @NonNull Map<String, FieldType> getFields() {
        return FIELDS_DEFINITION.get();
    }

    @Override
    public @NonNull Map<String, Object> map(@NonNull Contact object) {
        return FIELDS_VALUE_MAPPER.map(object);
    }

    @Override
    public @NonNull String getEntityName() {
        return ENTITY_NAME;
    }

    @Override
    public void create(@NonNull Contact object, @NonNull Callback<Long> callback) {

    }

    @Override
    public  void read(long id, @NonNull Callback<Contact> callback) {

    }

    @Override
    public void readAll(@NonNull Callback<Collection<Contact>> callback) {

    }

    @Override
    public void update(@NonNull Contact object, @NonNull Callback<Void> callback) {

    }

    @Override
    public void delete(@NonNull Contact object, @NonNull Callback<Void> callback) {

    }

    @Override
    public @NonNull Lazy<Contact> getLazyInstance(long id) {
        return null;
    }

    @Override
    public @NonNull Lazy<Collection<Contact>> getLazyCollectionInstance() {
        return null;
    }
}
