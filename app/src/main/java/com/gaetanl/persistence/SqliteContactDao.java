package com.gaetanl.persistence;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.gaetanl.domain.Contact;
import com.gaetanl.domain.Lazy;
import com.gaetanl.util.Callback;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Contact SQLite concrete Dao implementation
 *
 * @author Gaetan Leu
 */
public class SqliteContactDao extends SqliteDao<Contact> implements ContactDao {
    private static SqliteContactDao INSTANCE = null;
    private static Object mutex = new Object();

    private SqliteContactDao(SQLiteOpenHelper dbHandler) {
        super(dbHandler);
    }

    public static SqliteContactDao getInstance(SQLiteOpenHelper dbHandler) {
        if (INSTANCE == null) {
            synchronized (mutex) {
                if (INSTANCE == null) {
                    INSTANCE = new SqliteContactDao(dbHandler);
                }
            }
        }

        return INSTANCE;
    }

    @Override
    public @NonNull Map<String, FieldType> getFields() {
        return FIELDS_DEFINITION.get();
    }

    @Override
    public @NonNull Map<String, Object> map(@NonNull Contact object) {
        return FIELDS_VALUE_MAPPER.map(object);
    }

    @Override
    public @NonNull String getEntityName() {
        return ENTITY_NAME;
    }

    @Override
    public void create(@NonNull Contact object, @NonNull Callback<Long> callback) {
        ContentValues contentValues = new ContentValues();
        Map<String, Object> values = map(object);
        for (Map.Entry<String, FieldType> field: getFields().entrySet()) {
            String fieldName = field.getKey();
            FieldType fieldType = field.getValue();
            Object value = values.get(fieldName);
            fieldType.putContentValue(contentValues, fieldName, value);
        }

        SQLiteDatabase db = dbHandler.getWritableDatabase();
        long id = db.insert(getEntityName(), null, contentValues);
        db.close();

        if (id == -1) {
            callback.onError(new Exception()); // TODO
        }
        else {
            callback.onResponse(id);
        }
    }

    @Override
    public void read(long id, @NonNull Callback<Contact> callback) {
        Contact contact;
        SQLiteDatabase db = dbHandler.getReadableDatabase();
        Cursor cursor = db.query(
                getEntityName(),
                new String[] {"displayedName"},
                " id = ?",
                new String[] {String.valueOf(id)},
                null, // group by
                null, // having
                null, // order by
                null); // limit

        if (cursor != null) {
            cursor.moveToFirst();
            String displayedName = cursor.getString(0);
            contact = new Contact(id, displayedName);

            callback.onResponse(contact);
            cursor.close();
        }
        else {
            callback.onError(new Exception()); // TODO
        }
    }

    @Override
    public void readAll(@NonNull Callback<Collection<Contact>> callback) {
        List<Contact> contacts = new LinkedList<Contact>();
        String query = "SELECT id, displayedName FROM " + getEntityName();
        SQLiteDatabase db = dbHandler.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    long id = Integer.parseInt(cursor.getString(0));
                    String displayedName = cursor.getString(1);
                    contacts.add(new Contact(id, displayedName));
                }
                while (cursor.moveToNext());
            }

            callback.onResponse(contacts);
            cursor.close();
        }
        else {
            callback.onError(new Exception()); // TODO
        }
    }

    @Override
    public void update(@NonNull Contact object, @NonNull Callback<Void> callback) {
        SQLiteDatabase db = dbHandler.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("displayedName", object.getDisplayedName());

        int nbUpdatedRows = db.update(
                getEntityName(),
                values,
                "id = ?",
                new String[] {String.valueOf(object.getId())});
        db.close();

        switch (nbUpdatedRows) {
            case 1:
                callback.onResponse(null);
                break;
            case 0:
                callback.onError(new Exception()); // TODO
                break;
            default:
                callback.onError(new Exception()); // TODO
                break;
        }
    }

    @Override
    public void delete(@NonNull Contact object, @NonNull Callback<Void> callback) {
        SQLiteDatabase db = dbHandler.getWritableDatabase();
        int nbRowsDeleted = db.delete(
                getEntityName(),
                "id = ?",
                new String[] {String.valueOf(object.getId())});
        db.close();

        switch (nbRowsDeleted) {
            case 1:
                callback.onResponse(null);
                break;
            case 0:
                callback.onError(new Exception()); // TODO
                break;
            default:
                callback.onError(new Exception()); // TODO
                break;
        }
    }

    @Override
    public @NonNull Lazy<Contact> getLazyInstance(final long id) {
        return new Lazy<Contact>() {
            private boolean localObjectUpToDate = false;

            @Override
            protected void update(@NonNull final Callback<Void> callback) {
                SqliteContactDao.this.read(id, new Callback<Contact>() { // TODO id?
                    @Override
                    public void onResponse(@NonNull Contact contact) {
                        setLocalObject(contact);
                        localObjectUpToDate = true;
                        callback.onResponse(null);
                    }

                    @Override
                    public void onError(@NonNull Throwable throwable) {
                        callback.onError(throwable);
                    }
                });
            }

            @Override
            protected boolean isLocalObjectUpToDate() {
                return localObjectUpToDate;
            }

            @Override
            public void setLocalObjectObsolete() {
                localObjectUpToDate = false;
            }
        };
    }

    @Override
    public @NonNull Lazy<Collection<Contact>> getLazyCollectionInstance() {
        return new Lazy<Collection<Contact>>() {
            private boolean localObjectUpToDate = false;

            @Override
            protected void update(@NonNull final Callback<Void> callback) {
                SqliteContactDao.this.readAll(new Callback<Collection<Contact>>() {
                    @Override
                    public void onResponse(@NonNull Collection<Contact> contacts) {
                        setLocalObject(contacts);
                        localObjectUpToDate = true;
                        callback.onResponse(null);
                    }

                    @Override
                    public void onError(@NonNull Throwable throwable) {
                        callback.onError(throwable);
                    }
                });
            }

            @Override
            protected boolean isLocalObjectUpToDate() {
                return localObjectUpToDate;
            }

            @Override
            public void setLocalObjectObsolete() {
                localObjectUpToDate = false;
            }
        };
    }
}
